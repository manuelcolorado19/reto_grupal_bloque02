class CreateQueries < ActiveRecord::Migration[5.2]
  def change
    create_table :queries do |t|
      t.string :name, null: false
      t.string :query, null: false 
      t.string :file_path, null: false 
    end
  end
end
