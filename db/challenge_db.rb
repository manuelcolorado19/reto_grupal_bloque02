require 'active_record'

class ChallengeDB < ActiveRecord::Base
  CONFIG = YAML.load_file(File.join("challenge_db_config.yml"))["development"]
  self.abstract_class = true
  establish_connection(CONFIG)

  def self.run_sql(query)
    connection.execute(query)
  end
end


ChallengeDB.run_sql("Select * from users")