require 'active_record'
require_relative '../db/connection'
require_relative '../db/challenge_db'
require_relative '../lib/lector_reportes/query'
require_relative '../lib/lector_reportes/placeholder'
module Main 
  
  def get_new_sql_file
    file_db=Query.select(:file_path).map(&:file_path)
    entries= Dir['../test/*.sql']
    new_ones = entries - file_db
    if new_ones.size > 0
      new_ones.each_with_index do |new_one,x|  
        puts "tienes #{new_ones.size-x} archivo(s) nuevo(s), ¿cómo deseas nombrarlo(s)?"
        file_name = gets.chomp
        arch = read_file new_one
        arch=arch.join("")
        save_query file_name,arch,new_one
        prepared_query = replace_placeholders arch 
        execute_query prepared_query
        gets
        #new_ones.shift
      end
    else
      puts "no tienes nuevos reportes"
    end 
  end

  def read_file entries
    arch=File.open(entries)
    nuevo=""
    arch.each {|f| nuevo=nuevo+f}  
    arch.close
    salida=nuevo.split("\n")
    salida
  end 

  def save_query file_name,arch,new_one
    Query.create!(name:file_name,query:arch,file_path:new_one)
    get_placeholer arch       
  end


  def get_placeholer arch
    this_query= Query.last
    arch = arch.split(" ")
    arch.each do |element|
      read=element.chars
      place=""
      repeat=0
      read.each do |inside|    
        repeat=+1 if inside == "@" 
        if repeat>0 && repeat <= 2
          place=place+inside 
        end   
      end
      filtered=place.split("@")
      Placeholder.create!(name:place, queries_id:this_query.id) unless place==""
    end
  end

  def replace_placeholder arch 
    #filtered = arch.split(/(?<=[@])/)
    filtered = arch.chars
    puts filtered
    gets
    times = 0
    ocurrencia = 0 
    (filtered.size-1).times do |time|
      times+=1 if filtered[time] == "@"
      if times == 1 
        #borro la posicion con la primera ocurrencia
        delete = time -ocurrencia
        filtered.delete_at(delete) # filtered[time] = d
        #incremento la ocurrencia para la proxima iteracion 
        ocurrencia+=1
        # verifico si me encuentro con el delimitador en la proxima iteración
        if filtered[delete] == "@"
          puts "ingrese el primer valor"
          value=gets.chomp
          # sustituyo el valor del delimitador
          filtered[delete]=value
          times = 0 
          ocurrencia = 0 
        end
      end
    end 
    query=filtered.join("")
  end

end 

include Main
get_new_sql_file